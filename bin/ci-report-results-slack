#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'fileutils'
require 'json'
require 'optimist'
require 'pathname'
require 'results'
require 'sitespeed_common'

results_dir = Pathname.new(File.expand_path('../results', __dir__)).relative_path_from(Dir.pwd)
sitespeed_dir = Pathname.new(File.expand_path('../sitespeed', __dir__)).relative_path_from(Dir.pwd)

@opts = Optimist.options do
  banner "Usage: ci-report-results-slack [options]"
  banner "\nReports GitLab SiteSpeed Performance test results to Slack. Designed for use in GitLab CI."
  banner "\nOptions:"
  opt :help, 'Show help message'
  opt :urls_config, "Name of URLs Config file in urls directory that contains the list of URLs to test with.", type: :string, default: "gpt.json"
  opt :environment, "Name of Environment Config file in environments directory that the test(s) will be run with. Alternative filepath can also be given.", type: :string, default: "10k.json"
  opt :channel, "Slack channel to post results to.", type: :string, default: "gpt-performance-run"
  opt :dry_run, "Only generate Slack message locally and don't post for testing purposes.", type: :flag
  banner "\nEnvironment Variable(s):"
  banner "  ENVIRONMENT_NAME                    Name of environment. (Default: nil)"
  banner "  SLACK_BOT_TOKEN                     A valid Slack Token that belongs to a Bot that has permissions for the intended Slack instance. (Default: nil)"
end

raise 'Environment Variable SLACK_BOT_TOKEN must be set to proceed. See command help for more info' unless ENV['SLACK_BOT_TOKEN'] || @opts[:dry_run]

env_file = Dir.glob([@opts[:environment], "#{sitespeed_dir}/environments/#{@opts[:environment]}", "#{sitespeed_dir}/environments/#{@opts[:environment]}.json"])[0]
raise "Environment Config file '#{@opts[:environment]}' not found as given or in default folder. Exiting..." unless File.file?(env_file.to_s)

env_data = SiteSpeedCommon.get_env_data(env_file)

def post_slack_snippet(message, content = nil)
  # Create a temporary file with the content
  require 'tempfile'
  temp_file = Tempfile.new(['sitespeed_results', '.txt'])
  begin
    temp_file.write(content)
    temp_file.close

    # Prepare parameters
    filename = ENV['ENVIRONMENT_NAME'] || 'sitespeed_results.txt'
    file_size = File.size(temp_file.path)
    title = ENV['ENVIRONMENT_NAME'] || 'SiteSpeed Results'

    # Step 1: Get upload URL
    upload_url_response = SiteSpeedCommon.make_http_request(
      method: 'post',
      url: "https://slack.com/api/files.getUploadURLExternal",
      params: {
        token: ENV['SLACK_BOT_TOKEN'],
        filename:,
        length: file_size
      }
    )

    # Parse the JSON response
    upload_url_data = JSON.parse(upload_url_response)
    unless upload_url_data['ok']
      puts "Error getting upload URL: #{upload_url_data['error']}"
      return false
    end

    # Step 2: Upload file to the provided URL
    file_upload_response = SiteSpeedCommon.make_http_request(
      method: 'post',
      url: upload_url_data['upload_url'],
      params: {
        token: ENV['SLACK_BOT_TOKEN']
      },
      body: File.read(temp_file.path),
      headers: { 'Content-Type' => 'text/plain' }
    )

    unless file_upload_response.status.success?
      puts "Error uploading file: #{file_upload_response.status}"
      puts "Response body: #{file_upload_response.body}" if file_upload_response.body
      return false
    end

    # Step 3: Complete the upload
    complete_upload_response = SiteSpeedCommon.make_http_request(
      method: 'post',
      url: "https://slack.com/api/files.completeUploadExternal",
      params: {
        token: ENV['SLACK_BOT_TOKEN'],
        files: [{ id: upload_url_data['file_id'], title: }].to_json,
        channel_ids: @opts[:channel],
        initial_comment: message
      }
    )

    # Parse the JSON response
    complete_upload_data = JSON.parse(complete_upload_response)
    unless complete_upload_data['ok']
      puts "Error completing upload: #{complete_upload_data['error']}"
      return false
    end

    puts "Results successfully uploaded to Slack"
    file_url = complete_upload_data['files'][0]['permalink']
    message_with_file = "#{message}\n• <#{file_url}|Results file>"
    SiteSpeedCommon.make_http_request(
      method: 'post',
      url: "https://slack.com/api/chat.postMessage",
      params: {
        token: ENV['SLACK_BOT_TOKEN'],
        channel: @opts[:channel],
        text: message_with_file
      }
    )
  ensure
    # Clean up the temporary file
    temp_file.unlink
  end
end

results_file = Dir.glob("#{results_dir}/data/performance.json")[0]
raise "SiteSpeed Results file 'performance.json' not found as given or in default folder. Exiting..." unless File.file?(results_file.to_s)

urls_config_file = Dir.glob([@opts[:urls_config], "#{sitespeed_dir}/urls/#{@opts[:urls_config]}", "#{sitespeed_dir}/urls/#{@opts[:urls_config]}.json"])[0]
raise "URLs Config file '#{@opts[:urls_config]}' not found as given or in default folder. Exiting..." unless File.file?(urls_config_file.to_s)

urls_config = JSON.parse(File.read(urls_config_file))
results = Results.get_tests_results(results_file:, urls_config:)

puts "Posting SiteSpeed test result summary and uploading results to Slack:\n#{results_file}"

message = "SiteSpeed :sitespeed: test against #{ENV['ENVIRONMENT_NAME']&.capitalize} has"

summary = if ENV['CI_IGNORE_RESULT']
            ":checkered_flag: #{message} finished! :checkered_flag:"
          else
            Results.get_aggregated_results(results) ? ":ci_passing: #{message} passed! :ci_passing:" : ":ci_failing: #{message} failed! :ci_failing:"
          end

summary << "\n\n• Pipeline - #{ENV['CI_PIPELINE_URL'].sub(/^https?:\/\/(www.)?/, '')}" if ENV['CI_PIPELINE_URL']
summary << "\n• SiteSpeed Report - #{ENV['SITESPEED_JOB_URL'].sub(/^https?:\/\/(www.)?/, '')}/artifacts/file/results/pages.html" if ENV['SITESPEED_JOB_URL']
summary << "\n• Results History Dashboard - #{ENV['SITESPEED_RESULTS_GRAFANA_DASHBOARD_URL'].sub(/^https?:\/\/(www.)?/, '')}" if ENV['SITESPEED_RESULTS_GRAFANA_DASHBOARD_URL']
summary << "\n• Docs - gitlab.com/gitlab-org/quality/performance-sitespeed#test-output-and-results"

results_table = Results.generate_results_table(test_results: results, urls_config:)
content = Results.generate_results_summary(env: env_data, results_table:)
puts content
post_slack_snippet(summary, content) unless @opts[:dry_run]
