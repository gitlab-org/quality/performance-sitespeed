#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
$stdout.sync = true

require 'fileutils'
require 'json'
require 'optimist'
require 'pathname'
require 'rainbow'
require 'sitespeed_common'

sitespeed_dir = Pathname.new(File.expand_path('../sitespeed', __dir__)).relative_path_from(Dir.pwd)

opts = Optimist.options do
  banner "\nUsage: warm-up-environment [options]"
  banner "Options:"
  opt :environment, "Name of Environment Config file in environments directory that the test(s) will be run with. Alternative filepath can also be given.", type: :string, required: true
  opt :urls_config, "Name of URLs Config file in urls directory that contains the list of URLs to test with.", type: :string, default: "gpt.json"
  opt :help, 'Show this help message'
  banner "\nExamples:"
  banner "   #{$PROGRAM_NAME} --environment staging.json"
end

# Variables
env_file = Dir.glob([opts[:environment], "#{sitespeed_dir}/environments/#{opts[:environment]}", "#{sitespeed_dir}/environments/#{opts[:environment]}.json"])[0]
raise "Environment Config file '#{opts[:environment]}' not found as given or in default folder. Exiting..." unless File.file?(env_file.to_s)

env_file_vars = JSON.parse(File.read(env_file))
environment_url = (ENV['ENVIRONMENT_URL'].dup || env_file_vars['url']).chomp('/')
environment_name = ENV['ENVIRONMENT_NAME'].dup || env_file_vars['name']

urls_config_file = Dir.glob([opts[:urls_config], "#{sitespeed_dir}/urls/#{opts[:urls_config]}", "#{sitespeed_dir}/urls/#{opts[:urls_config]}.json"])[0]
raise "URLs Config file '#{opts[:urls_config]}' not found as given or in default folder. Exiting..." unless File.file?(urls_config_file.to_s)

urls_config = JSON.parse(File.read(urls_config_file))

# Warm up
print Rainbow("Waiting for #{environment_name} to warm up...\nStart time: #{Time.now}\n").yellow
begin
  retries ||= 0
  gitlab_healthcheck = SiteSpeedCommon.make_http_request(method: 'get', url: "#{environment_url}/-/liveness", fail_on_error: false).status.success?

  homepage_healthcheck = true
  50.times do
    homepage = SiteSpeedCommon.make_http_request(method: 'get', url: environment_url, fail_on_error: false)
    homepage_healthcheck = !homepage.body.to_s.include?("GitLab environment is not available") && homepage.status.success?
    break if homepage_healthcheck

    sleep 1
  end

  urls_healthcheck = true
  10.times do
    urls_healthcheck = urls_config.reduce(true) do |aggregated, url_config|
      url_res = SiteSpeedCommon.make_http_request(method: 'get', url: "#{environment_url}#{url_config.last['url']}", fail_on_error: false)
      aggregated && url_res.status.success?
    end
    break if urls_healthcheck
  end

  print '.'
  raise "One or more healthchecks failed (GitLab: #{gitlab_healthcheck}, Homepage: #{homepage_healthcheck}, URLs: #{urls_healthcheck}" unless gitlab_healthcheck && homepage_healthcheck && urls_healthcheck
rescue RuntimeError, HTTP::ConnectionError => e
  sleep 5
  retries += 1
  raise e if retries > 5

  retry
rescue StandardError => e
  puts Rainbow("\nWarm up failed:\n #{e.message}\nTraceback:#{e.backtrace}\nFinish time: #{Time.now}").red
rescue Interrupt
  warn Rainbow("Caught the interrupt.").yellow
  exit
end

puts Rainbow("\nWarm up finished.\nFinish time: #{Time.now}").green
