#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'fileutils'
require 'influxdb'
require 'json'
require 'optimist'
require 'pathname'
require 'rainbow'
require 'report_wiki'
require 'results'
require 'sitespeed_common'

results_dir = Pathname.new(File.expand_path('../results', __dir__)).relative_path_from(Dir.pwd)
sitespeed_dir = Pathname.new(File.expand_path('../sitespeed', __dir__)).relative_path_from(Dir.pwd)

@opts = Optimist.options do
  banner "Usage: ci-report-results-wiki [options]"
  banner "\nReports GitLab SiteSpeed Performance test results to a Wiki. Designed for use in GitLab CI."
  banner "\nOptions:"
  opt :help, 'Show help message'
  opt :urls_config, "Name of URLs Config file in urls directory that contains the list of URLs to test with.", type: :string, default: "gpt.json"
  opt :environment, "Name of Environment Config file in environments directory that the test(s) will be run with. Alternative filepath can also be given.", type: :string, default: "10k.json"
  opt :results_path, "Path of SiteSpeed test results directory.", type: :string, default: results_dir.to_s
  opt :page_title, "Title to use for wiki page", type: :string
  opt :api_url, "GitLab wiki API URL", type: :string, default: "https://gitlab.com/api/v4/projects/gitlab-org%2Fquality%2Fperformance/wikis"
  opt :influxdb_url, "URL of an Influx DB server where GitLab Performance SiteSpeed can optionally upload test run statistics.", type: :string, default: ENV['SITESPEED_INFLUXDB_URL'] || nil
  opt :dry_run, "Only generate Wiki page locally and don't post for testing purposes.", type: :flag
  banner "\nEnvironment Variable(s):"
  banner "  CI_PROJECT_ACCESS_TOKEN   A valid GitLab Personal Access Token that has access to the intended project where the wiki page will be posted. The token should come from a User that has admin access for the project(s) and have all permissions. (Default: nil)"
end

raise 'Environment Variable CI_PROJECT_ACCESS_TOKEN must be set to proceed. See command help for more info' unless ENV['CI_PROJECT_ACCESS_TOKEN'] || @opts[:dry_run]
raise 'Page Title must be specified' unless @opts[:page_title] || @opts[:dry_run]

results_file = Dir.glob("#{@opts[:results_path]}/data/performance.json")[0]
raise "SiteSpeed Results file 'performance.json' not found as given or in default folder. Exiting..." unless File.file?(results_file.to_s)

env_file = Dir.glob([@opts[:environment], "#{sitespeed_dir}/environments/#{@opts[:environment]}", "#{sitespeed_dir}/environments/#{@opts[:environment]}.json"])[0]
raise "Environment Config file '#{@opts[:environment]}' not found as given or in default folder. Exiting..." unless File.file?(env_file.to_s)

env_data = SiteSpeedCommon.get_env_data(env_file)

urls_config_file = Dir.glob([@opts[:urls_config], "#{sitespeed_dir}/urls/#{@opts[:urls_config]}", "#{sitespeed_dir}/urls/#{@opts[:urls_config]}.json"])[0]
raise "URLs Config file '#{@opts[:urls_config]}' not found as given or in default folder. Exiting..." unless File.file?(urls_config_file.to_s)

urls_config = JSON.parse(File.read(urls_config_file))
results = Results.get_tests_results(results_file:, urls_config:)
results_table = Results.generate_results_table(test_results: results, urls_config:)
results_summary = Results.generate_results_summary(env: env_data, results_table:)

results_contents = "## Test Links"
results_contents << "\n* [Pipeline](#{ENV['CI_PIPELINE_URL']})" if ENV['CI_PIPELINE_URL']
results_contents << "\n* [SiteSpeed Report](#{ENV['SITESPEED_JOB_URL']}/artifacts/file/results/pages.html)" if ENV['SITESPEED_JOB_URL']
results_contents << "\n* [Results History Dashboard](#{ENV['SITESPEED_RESULTS_GRAFANA_DASHBOARD_URL']})" if ENV['SITESPEED_RESULTS_GRAFANA_DASHBOARD_URL']
results_contents << "\n* [Docs](https://gitlab.com/gitlab-org/quality/performance-sitespeed#test-output-and-results)"
results_contents << "\n* Grafana dashboard login credentials are stored in `Engineering` 1Password vault under `Performance Grafana dashboard credentials`" if ENV['SITESPEED_RESULTS_GRAFANA_DASHBOARD_URL']
results_contents << "\n* [GitLab Commit SHA](https://gitlab.com/gitlab-org/gitlab/-/commits/#{env_data['revision']})" unless env_data['revision'].include?('-')
results_contents << "\n## Test Results\n\n#{results_summary}"
# Colored results highlight for wiki
results_contents.gsub!(/Passed/, '**{+Passed+}**') unless @opts[:dry_run]
results_contents.gsub!(/FAILED/, '**{-FAILED-}**') unless @opts[:dry_run]

results_contents_file = File.join(@opts[:results_path], "results_contents.txt")
puts "Saving results to #{results_contents_file}"
File.write(results_contents_file, results_contents)

puts results_contents

if @opts[:influxdb_url]
  influxdb_report, message = InfluxDB.write_data(@opts[:influxdb_url], results, env_data)
  warn Rainbow("\nFailed to upload test run statistics to InfluxDB URL #{@opts[:influxdb_url]} - #{message}.").red unless influxdb_report
end

exit if @opts[:dry_run]

ReportWiki.report_to_wiki(api_url: @opts[:api_url], page_title: @opts[:page_title], access_token: ENV['CI_PROJECT_ACCESS_TOKEN'], report_contents: results_contents)
