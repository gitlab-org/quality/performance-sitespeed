require 'http'
require 'json'
require 'table_print'

module SiteSpeedCommon
  extend self

  def make_http_request(method: 'get', url: nil, params: {}, headers: {}, body: "", show_response: false, fail_on_error: true)
    raise "URL not defined for making request. Exiting..." unless url

    ctx = OpenSSL::SSL::SSLContext.new
    ctx.verify_mode = OpenSSL::SSL::VERIFY_NONE

    res = body.empty? ? HTTP.follow.method(method).call(url, form: params, headers:, ssl_context: ctx) : HTTP.follow.method(method).call(url, body:, headers:, ssl_context: ctx)

    if show_response
      res_body = res.content_type.mime_type == "application/json" ? JSON.parse(res.body.to_s) : res.body.to_s
      puts res_body
    end

    raise "#{method.upcase} request failed!\nCode: #{res.code}\nResponse: #{res.body}\n" if fail_on_error && !res.status.success?

    res
  end

  def get_env_version(env_url:)
    headers = { 'PRIVATE-TOKEN': ENV['ACCESS_TOKEN'] }
    res = make_http_request(method: 'get', url: "#{env_url}/api/v4/version", headers:, fail_on_error: false)
    res.status.success? ? JSON.parse(res.body.to_s) : { "version" => "-", "revision" => "-" }
  end

  def get_env_data(env_file)
    env_file_vars = JSON.parse(File.read(env_file))
    gitlab_version = SiteSpeedCommon.get_env_version(env_url: env_file_vars['url'])
    {
      "name" => env_file_vars['name'],
      "version" => gitlab_version['version'],
      "revision" => gitlab_version['revision'],
      "date" => Time.now.strftime('%F')
    }
  end
end
