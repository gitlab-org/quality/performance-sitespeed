require 'sitespeed_common'
require 'uri'

module InfluxDB
  extend self

  def healthy?(influxdb_host)
    SiteSpeedCommon.make_http_request(method: 'get', url: "#{influxdb_host}/ping?verbose=true", fail_on_error: false).status.success?
  rescue HTTP::ConnectionError, Errno::ECONNREFUSED
    false
  end

  def prepare_request_body(measurement, tags, value, time)
    # https://docs.influxdata.com/influxdb/v1.7/guides/writing_data/#write-data-using-the-influxdb-api
    "#{measurement},#{tags.map { |h| h.join '=' }.join ','} value=#{value} #{time}"
  end

  def prepare_request_data(results_json, env_data)
    tests_end_time = (Time.now.to_f * 1000).to_i * 1_000_000 # The timestamp for InfluxDB data point in nanosecond-precision Unix time.
    tags = {
      gitlab_version: env_data['version'],
      gitlab_revision: env_data['revision']
    }
    measurements = %w[speed_index LVC FCP LCP TBT transfer_size coach_performance_score]
    results_json.map do |test_result|
      measurements.map do |measurement|
        prepare_request_body(measurement, tags.merge({ test_name: test_result['name'] }), test_result[measurement], tests_end_time)
      end.join("\n")
    end.join("\n")
  end

  def write_data(influxdb_url, results_json, env_data)
    return false, "Invalid URL" unless influxdb_url.match?(URI::DEFAULT_PARSER.make_regexp)

    influxdb_host = influxdb_url.match(/(.*)\//)[1]
    influxdb_db = influxdb_url.split("/")[-1]
    influxdb_write_url = "#{influxdb_host}/write?db=#{influxdb_db}"

    return false, "URL can’t be reached" unless healthy?(influxdb_host)

    body = prepare_request_data(results_json, env_data)
    SiteSpeedCommon.make_http_request(method: 'post', url: influxdb_write_url, body:, fail_on_error: false)
  end
end
