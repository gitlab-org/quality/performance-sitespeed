# GitLab Browser Performance Tool

SiteSpeed wrapper for GitLab Browser Performance testing.

To get more detailed information about the current test pages list you can refer to the [Current Test Details wiki page](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/wikis/Current-Test-Details).

[[_TOC_]]

## Preparing Tests

To run SiteSpeed tests they require a text file list of URLs to test with. This file can be either created manually or prepared with the URL Generator tool.

### Preparing the Environment File

An [Environment Config File](./sitespeed/environments) is used by URL Generator tool to generate text file list of URLs that SiteSpeed will use. This file contains details about the environment to be tested.

Typically, all that's needed to be done for this file is to copy one of the existing examples and tweak it accordingly for your environment. As an example here is one of the example files available, [`10k.json`](./sitespeed/environments/10k.json), with details after on what specific sections you would and would not typically need to tweak:

```json
{
  "name": "10k",
  "url": "https://10k.testbed.gitlab.net"
}
```

Details for each of the settings are as follows:

* `name` - The name of the environment. (Environment variable: `ENVIRONMENT_NAME`)
* `url` - Full URL of the environment. (Environment variable: `ENVIRONMENT_URL`)

### Running the URL Generator tool

When the environment config file is in place the [URL Generator](./bin/generate-sitespeed-urls) tool can now be run to setup the data for the SiteSpeed.

Before running some setup is required for the GPT Data Generator tool specifically:

1. First, set up [`Ruby`](https://www.ruby-lang.org/en/documentation/installation/) and [`Ruby Bundler`](https://bundler.io) if they aren't already available on the machine.
    * [Ruby version](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/blob/master/.ruby-version)
    * Bundler version specified in [Gemfile.lock](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/blob/master/Gemfile.lock) below the text `BUNDLED WITH`.
1. Next, install the required Ruby Gems via Bundler
    * `bundle install`
1. If you made a custom [environment config file](#preparing-the-environment-file), add it to the `environments` directory in the tool's root folder: [`sitespeed/environments`](./sitespeed/environments).

Once setup is done you can run the tool with the `bin/generate-sitespeed-urls` script. The full options for running the tool can be seen by getting the help output via `bin/generate-sitespeed-urls --help`:

```text
Usage: generate-sitespeed-urls [options]
Options:
  -e, --environment=<s>    Name of Environment Config file in environments directory that the test(s) will be run with. Alternative filepath can also be given.
  -u, --urls-config=<s>    Name of URLs Config file in urls directory that contains the list of URLs to test with. (Default: gpt.json)
  -h, --help               Show this help message

Examples:
   bin/generate-sitespeed-urls --environment staging.json
```

By default, the tool will use the [`gpt.json`](./sitespeed/urls/gpt.json) URLs Config file to generate URLs file for the SiteSpeed. Custom URLs Config file can also be provided.

After running the URLs file will be in the `urls` folder.

## Preparing SiteSpeed

In addition to the test setup above there are some steps required to prepare SiteSpeed.

### SiteSpeed Configuration files

You can also specify a [config file to pass to SiteSpeed itself](https://www.sitespeed.io/documentation/sitespeed.io/configuration/#configuration-as-json). A few settings are already configured in this project's [`config.json`](./config.json) file:

```json
{
  "browsertime": {
    "browser": "chrome",
    "cpu": true,
    "iterations": "2",
    "screenshot": true,
    "screenshotParams": {
      "type": "jpg"
    },
    "videoParams": {
      "createFilmstrip": false
    },
    "viewPort": "1366x1536"
  },
  "budget": {
    "configPath": "budget.json",
    "suppressExitCode": true
  },
  "summary-detail": true,
   "axe": {
    "enable": true
  }
}
```

The full list of SiteSpeed config options can be found [here](https://www.sitespeed.io/documentation/sitespeed.io/configuration/#the-options).

In addition to this is the [Budget file (`budget.json`)](./budget.json), which specifies several test thresholds for all pages. Refer to the SiteSpeed [Budget documentation](https://www.sitespeed.io/documentation/sitespeed.io/performance-budget/) to learn more.

### Preparing the Plugin

This tool comes with a custom [SiteSpeed plugin](./sitespeed/plugin/index.js) that can be used to export specific results into a JSON file that can in turn be used for reporting ([`ci-report-results-wiki`](./bin/ci-report-results-wiki) or [`ci-report-results-slack`](./bin/ci-report-results-slack) for example).

From SiteSpeed 27 onwards this plugin has a dependency on the [sitespeed.io plugin](https://github.com/sitespeedio/plugin/blob/main/plugin.js). As this is javascript based this plugin needs to be installed as a dependency via `npm`. This can be down by running the following command:

```sh
npm install --prefix ./sitespeed/plugin/
```

## Running Tests

The SiteSpeed tests are run via the [official Docker image](https://hub.docker.com/r/sitespeedio/sitespeed.io/).

Here is an example of the tests running against the Staging environment:

```bash
docker pull sitespeedio/sitespeed.io
docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io --config config.json --plugins.add ./sitespeed/plugin/index.js --preURL https://10k.testbed.gitlab.net/explore --outputFolder results urls/10k.txt
```

To run against a different environment [prepare URLs file](#preparing-tests) and change the text file given at the end of command accordingly.

## Test Output and Results

Results will be found on the host in the folder `results`, which will be located in the same directory as the one you used the command in.

Once all tests have completed you will be presented with the default [SiteSpeed results summary](https://www.sitespeed.io/examples/).

As a convenience we provide a way to generate a custom results table for the test run that highlights key metrics. To generate the table run `bin/ci-report-results-wiki --dry-run` command. As an example, here is a test summary for all tests done against the `10k` environment:

```txt
NAME                                  | FCP (ms) | LCP (ms)       | TBT (ms)       | SI (ms) | LVC (ms) | TFR SIZE (kb) | SCORE | RESULT
--------------------------------------|----------|----------------|----------------|---------|----------|---------------|-------|-------
web_group                             | 1203     | ✓ 1203 (<4000) | ✓ 284 (<700)   | 1867    | 3183     | 95.1          | 79    | Passed
web_project                           | 1814     | ✓ 1814 (<4000) | ✓ 639 (<1000)  | 2226    | 3800     | 307.6         | 71    | Passed
web_project_branches                  | 1668     | ✓ 1668 (<2500) | ✓ 127 (<300)   | 1701    | 2517     | 46.0          | 87    | Passed
web_project_commit                    | 1948     | ✓ 1948 (<2500) | ✓ 553 (<1000)  | 3410    | 5267     | 356.1         | 73    | Passed
web_project_commits                   | 1464     | ✓ 1464 (<2500) | ✓ 152 (<300)   | 1487    | 1767     | 77.8          | 75    | Passed
web_project_file_blame                | 3726     | ✓ 3726 (<4000) | ✓ 449 (<950)   | 3762    | 4716     | 372.7         | 77    | Passed
web_project_file_entire_blame         | 2196     | ✓ 2196 (<3000) | ✓ 2695 (<4000) | 2375    | 7483     | 666.3         | 77    | Passed
web_project_file_rendered             | 1680     | ✓ 5258 (<7300) | ✓ 1293 (<1400) | 2895    | 8417     | 1699.5        | 72    | Passed
web_project_file_source               | 1585     | ✓ 2900 (<4100) | ✓ 476 (<800)   | 2155    | 3483     | 1068.2        | 73    | Passed
web_project_files                     | 994      | ✓ 1816 (<2500) | ✓ 1378 (<1700) | 1646    | 4383     | 313.7         | 75    | Passed
web_project_issue                     | 1608     | ✓ 1608 (<6000) | ✓ 2203 (<3100) | 1866    | 5217     | 1290.2        | 72    | Passed
web_project_issues                    | 645      | ✓ 2807 (<3700) | ✓ 758 (<800)   | 1298    | 2950     | 321.9         | 74    | Passed
web_project_merge_request_changes     | 1365     | ✓ 4480 (<8000) | ✓ 2082 (<2100) | 3303    | 10866    | 1535.4        | 69    | Passed
web_project_merge_request_commits     | 1767     | ✓ 1767 (<3000) | ✓ 540 (<1000)  | 3023    | 5500     | 1039.5        | 74    | Passed
web_project_merge_request_discussions | 1455     | ✓ 1455 (<2500) | ✓ 4703 (<6300) | 2815    | 13800    | 1396.6        | 73    | Passed
web_project_merge_requests            | 1722     | ✓ 1722 (<3600) | ✓ 193 (<400)   | 1721    | 2200     | 273.1         | 77    | Passed
web_project_pipelines                 | 718      | ✓ 3512 (<4500) | ✓ 940 (<1600)  | 1752    | 3616     | 186.7         | 71    | Passed
web_project_releases                  | 475      | ✓ 1420 (<2600) | ✓ 452 (<600)   | 776     | 1417     | 421.2         | 83    | Passed
web_project_tags                      | 1348     | ✓ 1348 (<2500) | ✓ 99 (<300)    | 1354    | 1567     | 43.0          | 92    | Passed
web_user                              | 598      | ✓ 1759 (<3500) | ✓ 33 (<300)    | 2028    | 5383     | 84.0          | 80    | Passed
```

Through these tests we measure several modern key metrics, recommended by [Google and the W3C](https://web.dev/metrics/) that indicate how pages are performing. We also follow the recommended targets for these metrics where specified:

* `FCP` - [First Contentful Paint (FCP)](https://web.dev/fcp/) measures the time from when the page starts loading to when any part of the page's content is rendered on the screen.
* `LCP` - [Largest Contentful Paint (LCP)](https://web.dev/lcp/) reports the render time of the largest image or text block visible within the viewport. Recommended target - 2500ms. Endpoint's passing LCP threshold specified in parentheses.
* `TBT` - [Total Blocking Time (TBT)](https://web.dev/tbt/) measures the total amount of time when the main thread was blocked for long enough to prevent input responsiveness. Recommended target - 300ms. Endpoint's passing TBT threshold specified in parentheses.
* `SI` - [Speed Index (SI)](https://web.dev/speed-index/) measures how quickly content is visually displayed during page load.
* `LVC` - [Last Visual Change (LVC)](https://www.sitespeed.io/documentation/sitespeed.io/metrics/#last-visual-change) measures the time when something for the last time changes within the viewport.
* `TFR SIZE` - The [Transfer Size (TFR SIZE)](https://www.sitespeed.io/documentation/sitespeed.io/metrics/#transfer-size) over the wire in KB. Helps to evaluate response page size.
* `SCORE` - The [Coach Performance Score (SCORE)](https://www.sitespeed.io/documentation/coach/introduction/) given by SiteSpeed for the page.

SiteSpeed test results are posted on this [project's wiki](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/wikis/home) for transparency as well as allowing users to compare:

* [SiteSpeed Results](https://gitlab.com/gitlab-org/quality/performance-sitespeed/-/wikis/Benchmarks/SiteSpeed/1k) - Our automated CI pipelines run multiple times each week and will post their result summaries to this wiki each time and also in slack channel `#gitlab-browser-performance-testing`
